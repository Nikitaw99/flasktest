from flask import Flask, url_for, jsonify
from time import localtime
# from json import JSONEncoder
app = Flask(__name__)

@app.route('/')
def index():
    return open("index.html", mode='r').read()
#     return "<b>Hello</b> world!"

@app.route('/cool/')
def cool():
    return "this page is really cool"

@app.route('/time/')
def time_page():
    # z = JSONEncoder()
    data = dict()
    x = localtime()
    data["year"] = x.tm_year
    data["month"] = x.tm_mon
    data["mday"] = x.tm_mday
    return jsonify(data)
    # return z.encode(data)
